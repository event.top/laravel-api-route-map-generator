<?php

return [
    'enabled'               => true,
    'api_repository'        => base_path( 'app/API' ),
    'api_namespace_prefix'  => '\App\API\\',
];
