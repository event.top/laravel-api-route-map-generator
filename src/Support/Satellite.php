<?php

namespace EVT\API\Support;

class Satellite
{
    /**
     * @param $arr
     * @param int $req
     * @return string
     */
    public static function arrayToFile($arr, $req = 0)
    {
        $lf     = "\n";
        $tab    = '    ';
        $tt     = str_repeat($tab, $req);
        $str    = '';

        $escape = function ( $v )
        {
            return str_replace(
                ['\'', '\\'],
                ['\\\'', '\\\\'], /* '\ */
                $v
            );
        };

        if ( is_array( $arr ) )
        {
            if ( !$arr )
            {
                $str .= '[]';
            }
            else
            {
                $str .= '[' . $lf;
                foreach ( $arr as $k => $v )
                {
                    if ( is_string( $k ) )
                    {
                        $k = sprintf( '\'%s\'', $k );
                    }
                    $str .= sprintf(
                        $tt . $tab . '%s => %s,' . $lf,
                        $k,
                        static::arrayToFile( $v, $req + 1 )
                    );
                }
                $str .= $tt . ']';
            }
        }
        elseif ( is_string( $arr ) )
        {
            $str .= sprintf( '\'%s\'', $escape( $arr ) );
        }
        elseif ( is_numeric( $arr ) )
        {
            $str .= $arr;
        }
        elseif ( $arr === true )
        {
            $str .= 'true';
        }
        elseif ( $arr === false )
        {
            $str .= 'false';
        }
        elseif ($arr === null)
        {
            $str .= 'null';
        }
        else
        {
            $str .= sprintf(
                '\'%s\'',
                $escape(serialize($arr))
            );
        }

        return $str ?: 'null';
    }

    /**
     * @param $method
     *
     * @param $array
     */
    public static function setArray( $method, $array )
    {
        $string = static::arrayToFile( $array, 1 );
        return PHP_EOL . sprintf('    ->%s( %s )', $method, $string );
    }

    /**
     * @param $method
     *
     * @param $string
     */
    public static function setString( $method, $string )
    {
        return PHP_EOL . sprintf('    ->%s( \'%s\' )', $method, $string);
    }


}
