<?php

namespace EVT\API\Support;

use EVT\API\Support\Satellite;

class BaseRoute
{
    /**
     * Enabled will be generated to the map
     * 
     * @var boolean|callable
     */
    protected $enabled               = false;

    /**
     * File map to place route to
     * 
     * @var null|string|callable
     */
    protected $map                   = null;

    /**
     * Generated Route
     * 
     * @var null|string
     */
    protected $ROUTE                 = null;

    /**
     * The URI pattern the route responds to
     * 
     * @var null|string|callable
     */
    protected $uri                   = null;

    /**
     * Route name
     * 
     * @var null|string|callable
     */
    protected $name                  = null;

    /**
     * Request method
     * Supports get|post|delete|update|options|patch|any|match|resource|apiResource|view
     * 
     * @var null|string|callable
     */
    protected $method                = null;

    /**
     * Where condition
     * 
     * @var null|string|array|callable
     */
    protected $where                 = null;

    /**
     * Parameters conditions
     * 
     * @var null|string|array|callable
     */
    protected $parameters            = null;

    /**
     * Controller class and method in `ControllerName@methodName` format. Same as Laravel's `uses`
     * REQUIRED! We do not use closures
     *
     * @var null|string|callable
     */
    protected $controller            = null;

    /**
     * Middleware to use with route
     * 
     * @var null|string|array|callable
     */
    protected $middleware            = null;

    /**
     * Domain for route
     * 
     * @var null|string|callable
     */
    protected $domain                = null;

    /**
     * Namespace for controller property
     * 
     * @var null|string|callable
     */
    protected $namespace             = null;

    /**
     * Reditect from uri1 to uri2
     * 
     * @var null|array
     */
    protected $redirect              = null;

    /**
     * Flag, if redirect is permanent
     * 
     * @var boolean
     */
    protected $redirectIsPermanent   = true;

    /**
     * prefix for route name
     * 
     * @var null|string
     */
    protected $as                    = null;

    /**
     * Route group identifier
     * 
     * @var null|string
     */
    protected $group                 = null;

    /**
     * Route group identifier
     * 
     * @var null|string|callable
     */
    protected $prefix                 = null;

    /**
     * Insert `use` command with classes before route list
     * 
     * @var null|array
     */
    protected $useClasses             = [];

    /**
     * methods|properties assigned to the route instead of group
     * 
     * @var null|array
     */
    protected $perRoute               = null;

    /**
     * Callable methods|properties must set as is
     * domain|middleware|prefix
     * 
     * @var array
     */
    protected $callable               = [];

    /**
     * parts already generated to skip duplicates
     * 
     * @var array
     */
    protected $generatedParts         = [];

    /**
     * magic getter
     *
     * @return mixed
     */
    public function __get( $key )
    {
        return ( isset( $this->$key ) ) ? $this->$key : null;
    }

    /**
     * builds route string
     *
     * @return string
     */
    public function buildGroup()
    {
        if ( $this->isGroup() )
        {

            $return = [];
            $return[] =  PHP_EOL . '#################################################################' ;
            $return[] =  PHP_EOL . '###   ' . $this->group . '   ' . str_repeat( '#', 56-mb_strlen($this->group) );
            $return[] =  PHP_EOL . '#################################################################' . PHP_EOL;
            $return[] = 'Route';
            $return[] = $this->genDomain( true );
            $return[] = $this->genPrefix();
            $return[] = $this->genMiddleware();
            $return[] = $this->genNamespace();
            $return[] = $this->genAs();
            $return[] = $this->genName();
            $return[] = $this->genWhere();
            $return[] = $this->genParameters();
            $return[] = PHP_EOL . '    ->group(function()' . PHP_EOL . '    {' . PHP_EOL . '/*REPLACE_WITH_ROUTES*/' . PHP_EOL . '    })';

            return implode( '', $return ) . PHP_EOL . ';' . PHP_EOL;
        }
    }

    /**
     * builds route string
     *
     * @return string
     */
    public function buildRoute()
    {
        $return = [];
        $return[] =  PHP_EOL . '/* ' . $this->group . '|' . $this->name . ' */ ' . PHP_EOL;
        $return[] = 'Route';
        if ( $this->isGroup() )
        {
            $return[] = $this->genMethod( true );
            $return[] = $this->genPerRoute();
        }
        else
        {
            $return[] = $this->genDomain( true );
            $return[] = $this->genMiddleware();
            $return[] = $this->genMethod();
            $return[] = $this->genPrefix();
            $return[] = $this->genName();
            $return[] = $this->genWhere();
            $return[] = $this->genParameters();
        }
        $this->ROUTE = implode( '', $return );
    }

    /**
     * check if route is under group
     *
     * @return boolean
     */
    protected function isGroup()
    {
        return ( !empty( $this->group ) ) ? true : false;
    }

    /**
     * Appends parts that should be at rouute call instead of group call
     * 
     * @return string|null
     */
    protected function genPerRoute()
    {
        $return = [];
        if ( !empty( $this->perRoute ) )
        {
            if ( is_array( $this->perRoute ))
            {
                foreach( $this->perRoute as $call )
                {
                    $method = 'gen' . ucfirst( $call );
                    if ( method_exists( $this, $method ) )
                    {
                        $return[] = $this->$method();
                    }
                }
            }
            else if ( is_string( $this->perRoute ) )
            {
                $method = 'gen' . ucfirst( $this->perRoute );
                if ( method_exists( $this, $method ) )
                {
                    $return[] = $this->$method();
                }
            }
        }
        return implode( '', $return );
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genMethod( $isFirst = false )
    {
        if ( in_array( 'method', $this->generatedParts ) )
        {
            return '';
        }
        $return = PHP_EOL;
        switch( $this->method )
        {
            case 'post':        $return .= '    ->post( \'';         break;
            case 'delete':      $return .= '    ->delete( \'';       break;
            case 'update':      $return .= '    ->update( \'';       break;
            case 'options':     $return .= '    ->options( \'';      break;
            case 'patch':       $return .= '    ->patch( \'';        break;
            case 'any':         $return .= '    ->any( \'';          break;
            case 'match':       $return .= '    ->match( \'';        break;
            case 'resource':    $return .= '    ->resource( \'';     break;
            case 'apiResource': $return .= '    ->apiResource( \'';  break;
            case 'view':        $return .= '    ->view( \'';         break;
            case 'get':
            default:            $return .= '    ->get( \'';          break;
        }
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        $return .= $this->uri . '\', ';
        $this->generatedParts[] = 'method';
        if ( empty( $this->controller ) )
        {
            $this->error( 'protected $controller can not be empty! It is Required option!' );
            die;
        }
        if ( in_array( 'controller', $this->generatedParts ) )
        {
            return '';
        }
        if ( !is_null( $this->namespace ) && !$this->isGroup() )
        {
            $return .= '\'' . $this->genNamespace( false, true ) . '\\' . $this->controller.'\' )';
        }
        else
        {
            $return .= '\''.$this->controller.'\' )';
        }
        $this->generatedParts[] = 'controller';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genName( $isFirst = false )
    {
        if ( empty( $this->name ) || in_array( 'name', $this->generatedParts ) )
        {
            return '';
        }
        $return                 = Satellite::setString( 'name', $this->name );
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        $this->generatedParts[] = 'name';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genWhere( $isFirst = false )
    {
        if ( empty( $this->where ) || in_array( 'where', $this->generatedParts ) || count( $this->where ) != 2 )
        {
            return '';
        }
//        $return                 = Satellite::setArray( 'where', $this->where );
        $return = PHP_EOL . '    ->where( \'' . $this->where[0] . '\', \'' . $this->where[1] . '\' )';
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        $this->generatedParts[] = 'where';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genNamespace( $isFirst = false, $partial = false )
    {
        if ( empty( $this->namespace ) || in_array( 'namespace', $this->generatedParts ) )
        {
            return '';
        }
        if ( $partial == true )
        {
            return $this->namespace;
        }
        else
        {
            $return                 = Satellite::setString( 'namespace', $this->namespace );
        }
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        $this->generatedParts[] = 'namespace';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genPrefix( $isFirst = false )
    {
        if ( empty( $this->prefix ) || in_array( 'prefix', $this->generatedParts ) )
        {
            return '';
        }
        $return                 = Satellite::setString( 'prefix', $this->prefix );
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        if ( in_array( 'prefix', $this->callable ) )
        {
            $this->setCallable( $return );
        }
        $this->generatedParts[] = 'prefix';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genAs( $isFirst = false )
    {
        if ( empty( $this->as ) || in_array( 'as', $this->generatedParts ) )
        {
            return '';
        }
        $return                 = Satellite::setString( 'as', $this->as );
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        $this->generatedParts[] = 'as';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genMiddleware( $isFirst = false )
    {
        if ( empty( $this->middleware ) || in_array( 'middleware', $this->generatedParts ) )
        {
            return '';
        }
        $return                 = Satellite::setArray( 'middleware', $this->middleware );
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        if ( in_array( 'middleware', $this->callable ) )
        {
            $this->setCallable( $return );
        }
        $this->generatedParts[] = 'middleware';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genParameters( $isFirst = false )
    {
        if ( empty( $this->parameters ) || in_array( 'parameters', $this->generatedParts ) )
        {
            return '';
        }
        $return                 = Satellite::setArray( 'parameters', $this->parameters );
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        $this->generatedParts[] = 'parameters';
        return $return;
    }

    /**
     * @param bool $isFirst
     * @return string
     */
    protected function genDomain( $isFirst = false )
    {
        if ( empty( $this->domain ) || in_array( 'domain', $this->generatedParts ) )
        {
            return '';
        }
        $return                 = Satellite::setString( 'domain', $this->domain );
        if ( $isFirst == true )
        {
            $this->setFirstCall( $return );
        }
        if ( in_array( 'domain', $this->callable ) )
        {
            $this->setCallable( $return );
        }
        $this->generatedParts[] = 'domain';
        return $return;
    }

    protected function setCallable( &$return )
    {
        $return = str_replace( "'", null, $return );
    }

    protected function setFirstCall( &$return )
    {
        $return = preg_replace( "/^([\s]+)/u", null, $return );
        $return = preg_replace( "/^(->)/u", '::', $return );
    }

}
