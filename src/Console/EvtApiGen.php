<?php

namespace EVT\API\Console;

use Illuminate\Console\Command;
use App\Models\Kpi\Kpi;
use App\User;

class EvtApiGen extends Command
{

    /**
     * Repository of mapped>groupped route objects to generate
     * @var array
     */
    protected $repository = [];

    /**
     * Grouped routes
     * @var array
     */
    protected $grouped = [];

    /**
     * UnGrouped routes
     * @var array
     */
    protected $ungrouped = [];

    /**
     * Using use
     * @var array
     */
    protected $useClasses = [];

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'zIntellect:api-gen {--api} {--web} {--console} {--broadcast}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Build API Routes and replace route maps"';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $starttime = microtime( 1 );
        $this->info( 'Route Maps Generator Start.' );

        $maps = [
            'api'       => $this->option( 'api' ),
            'web'       => $this->option( 'web' ),
            // 'console'   => $this->option( 'console' ),
            // 'broadcast' => $this->option( 'broadcast' ),
        ];

        if ( config('EVTAPI.enabled' ) == false )
        {
            $maps = [];
            $this->warn( 'Route Maps Generator DISABLED VIA CONFIG' );
        }

        $_grouped   = [];
        $_ungrouped = [];

        $this->_processFilesystem( $maps );
        $this->_processRepository( $_grouped, $_ungrouped );
        $this->_finalizeGrouped( $_grouped );
        $this->_finalizeUngrouped( $_ungrouped );
        $files = $this->_saveToTemp( $maps );
        if ( !empty( $files ) && $this->_checkValidity( $files ) )
        {
            $this->_replaceMapFiles( $files );
        }
        else
        {
            $this->error( 'Can not continue due to errors' );
        }
        $this->_cleanup( $files );

        $this->info( 'Route Maps Generator Done' );
        $this->comment('');

    }

    /**
     * Saving real map files
     *
     * @rapam array $files
     * @return void
     */
    protected function _replaceMapFiles( $files )
    {
        $this->info( 'Replacing the old maps with new generated' );
        if( !empty( $files ) )
        {
            foreach($files as $map => $file)
            {
                switch( $map )
                {
                    case 'api':
                        $filename = base_path() . '/routes/api.php';
                    break;
                    case 'web':
                        $filename = base_path() . '/routes/web.php';
                    break;
                    //case 'console':
                    //    $filename = base_path() . '/routes/console.php';
                    //break;
                    case 'broadcast':
                        $filename = base_path() . '/routes/channels.php';
                    break;
                }
                if ( !@copy( stream_get_meta_data($file)['uri'], $filename ) )
                {
                    $error = error_get_last();
                    $this->error( $error['type'] . ' :: ' . $error['message'] );
                }
            }
        }
    }

    /**
     * Saving to the temporary file to bechecked
     *
     * @rapam array $files
     * @return void
     */
    protected function _checkValidity( $files )
    {
        $this->info( 'Checking PHP Syntax' );
        $head = [ 'Map', 'File', 'Valid', 'LINT' ];
        $data = [];
        $errors = false;
        foreach( $files as $map => $file )
        {
            $filename = stream_get_meta_data($file)['uri'];
            if ( exec( 'php -l '.$filename, $msg ) )
            {
                $valid = 'yes';
            }
            else
            {
                $valid = 'no';
                $errors = true;
            }
            $data[] = [
                $map,
                str_replace( base_path() . '/', null, $filename ),
                $valid,
                implode( PHP_EOL, $msg )
            ];
        }
        $this->table( $head, $data );
        return !$errors;
    }

    /**
     * Savibg to the temporary file to bechecked
     *
     * @rapam array $maps
     * @return void
     */
    protected function _saveToTemp( &$maps )
    {
        $this->info( 'Saving maps to temp files' );
        $files = [];
        if ( !empty( $maps ) )
        {
            foreach( $maps as $map => $enabled )
            {
                $x = $y = false;
                if ( !empty( $enabled ) && !empty( $this->grouped[ $map ] ) )
                {
                    $x = true;
                }
                unset($group, $content);
                if ( !empty( $enabled ) && !empty( $this->ungrouped[ $map ] ) )
                {
                    $y = true;
                }
                if ( $x==true || $y==true )
                {
                    $files[ $map ] = tmpfile();
                    fwrite( $files[ $map ], '<?php' . PHP_EOL . PHP_EOL );
                    // USE's call
                    fwrite( $files[ $map ], 'use Illuminate\Support\Facades\Route;' . PHP_EOL );

                    if ( !empty( $this->useClasses[ $map ] ) )
                    {
                        $classes = 'use ' . implode( PHP_EOL . 'use ', $this->useClasses[ $map ] ) . ';' . PHP_EOL . PHP_EOL;
                        fwrite( $files[ $map ], $classes );
                    }

                    // PREPEND STUB FILE
                    $stubFile = base_path( 'app/API/_stubs' ) . '/' . $map . '.stub';
                    if ( is_file( $stubFile ) && is_readable( $stubFile ) )
                    {
                        $stubContent = file_get_contents( $stubFile );
                        fwrite( $files[ $map ], $stubContent );
                    }

                    if ( $x == true )
                    {
                        if ( !empty( $this->grouped[ $map ] ) )
                        {
                            foreach( $this->grouped[ $map ] as $group => $content )
                            {
                                fwrite( $files[ $map ], $content );
                            }
                        }
                    }
                    if ( $y == true )
                    {
                        if ( !empty( $this->ungrouped[ $map ] ) )
                        {
                            fwrite( $files[ $map ], $this->ungrouped[ $map ] );
                        }
                    }
                }
            }
        }
        foreach($files as $map => &$file )
        {
            $tmpfile_path = stream_get_meta_data($file)['uri'];
            $content      = file_get_contents($tmpfile_path);
            if ( empty( $content ) )
            {
                unset( $file );
                unset( $maps[ $map ] );
            }
        }
        return $files;
    }

    /**
     * remove temp files
     *
     * @rapam array $files
     * @return void
     */
    protected function _cleanup( $files )
    {
        if ( !empty( $files ) )
        {
            foreach( $files as $pointer )
            {
                fclose( $pointer );
            }
        }
    }

    /**
     * process Repository routes
     *
     * @rapam array $maps
     * @return void
     */
    protected function _processFilesystem( $maps )
    {
        $this->info('Reading Filesystem');
        $files = glob( config( 'EVTAPI.api_repository', base_path( 'app/API' ) ) . '/*.php' );
        $table_head = [ 'Class', 'Map', 'Group', 'domain', 'Route' ];
        $table_data = [];
        if ( !empty( $files ) )
        {
            foreach( $files as $file )
            {
                $class = config( 'EVTAPI.api_namespace_prefix' ) . basename( $file, '.php' );
                if ( class_exists( $class ) )
                {
                    $group_generated = $route_generated = false;
                    $map = $class::i()->map;
                    // do not generate route map if not asked
                    if ( empty( $maps [ $map ] ) )
                    {
                        continue;
                    }
                    //
                    $group = $class::i()->group;
                    //
                    if ( $class::i()->enabled == true )
                    {
                        $this->repository[ $map ][ $group ][] = $class::i();
                        $class::i()->buildRoute();
                        $route_generated = true;
                        if ( !empty( $group ) )
                        {
                            $this->grouped[ $map ][ $group ] = $class::i()->buildGroup();
                            $group_generated = true;
                        }
                    }
                    $table_data[] = [
                        str_replace( base_path() . '/', null, $file ),
                        $map,
                        $group,
                        // TODO: возможно дырка, надо подумать, как переделать
                        in_array( 'domain', $class::i()->callable ) ? eval( 'return ' . $class::i()->domain . ';' ) : $class::i()->domain . '+',
                        $class::i()->uri,
                    ];
                }
            }
            $this->info( 'Found enabled routes' );
            $this->table( $table_head, $table_data );
        }

    }

    /**
     * process Repository routes
     *
     * @rapam array $_grouped
     * @rapam array $_ungrouped
     * @return void
     */
    protected function _processRepository( &$_grouped, &$_ungrouped )
    {
        $this->info( 'Building route repository' );
        if ( !empty( $this->repository ) )
        {
            foreach( $this->repository as $map => $item )
            {
                if ( !empty( $item ) )
                {
                    foreach( $item as $group => $routes )
                    {
                        if ( !empty( $routes ) )
                        {
                            foreach( $routes as $_rn => $route )
                            {
                                if ( !empty( $group ) && isset( $this->grouped[ $map ][ $group ] ) )
                                {
                                    $_grouped[ $map ][ $group ][] = $route::i()->ROUTE . PHP_EOL . ';' . PHP_EOL;
                                }
                                else
                                {
                                    $_ungrouped[ $map ][] = $route::i()->ROUTE . PHP_EOL . ';' . PHP_EOL;
                                }
                                $classes = $route::i()->useClasses;
                                if ( !empty( $classes ) )
                                {
                                    if ( !isset( $this->useClasses[ $map ] ) )
                                    {
                                        $this->useClasses[ $map ] = [];
                                    }
                                    $this->useClasses[ $map ] = array_merge( $this->useClasses[ $map ], $route::i()->useClasses );
                                }
                                unset( $this->repository[ $map ][ $group ][ $_rn ] );
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Finalize Grouped routes
     *
     * @rapam array $_grouped
     * @return void
     */
    protected function _finalizeGrouped( &$_grouped )
    {
        if ( !empty( $_grouped ) )
        {
            foreach( $_grouped as $map => $groups )
            {
                if ( !empty( $groups ) )
                {
                    foreach( $groups as $group => $routes )
                    {
                        if ( !empty( $routes ) && is_array( $routes ) )
                        {
                            $_routes = [];
                            $_first = true;

                            foreach( $routes as $route )
                            {
/*
                                $_routes[] = $route;
                                $_spaces   = ( $_first == true ) ? str_repeat( ' ', 0 ) : str_repeat( ' ', 4 );
                                $_routes   = preg_replace( "/^/um", $_spaces, $_routes );
                                $_first    = false;
*/
//                                $_spaces   = ( $_first == true ) ? str_repeat( ' ', 0 ) : str_repeat( ' ', 4 );
//                                $route     = preg_replace( "/^/um", $_spaces, $route );
                                $_first    = false;
                                $route     = explode( "\n", $route );
                                array_walk( $route, function ( &$item, $key, $num = null )
                                {
                                    $item = ( !empty( $item ) )
                                        ? str_repeat( ' ', $num ) . $item
                                        : $item;
                                }, 8 );
                                $route     = implode( "\n", $route );
                                $_routes[] = $route;
                            }

                            $_routes = implode( '', $_routes );
                            $this->grouped[ $map ][ $group ] = str_replace( "/*REPLACE_WITH_ROUTES*/", $_routes, $this->grouped[ $map ][ $group ] );
                        }
                    }
                }
            }
        }
    }

    /**
     * Finalize UnGrouped routes
     *
     * @rapam array $_ungrouped
     * @return void
     */
    protected function _finalizeUngrouped( &$_ungrouped )
    {
        if ( !empty( $_ungrouped ) )
        {
            foreach( $_ungrouped as $map => $routes )
            {
                $_routes = [];
                if ( !empty( $routes ) )
                {
                    foreach( $routes as $route )
                    {
                        $_routes[] = $route;
                    }
                }
                $_routes = implode( '', $_routes );
                $this->ungrouped[ $map ] = $_routes;
            }
        }
    }

}
