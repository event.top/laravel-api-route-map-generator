<?php

namespace EVT\API;

use Illuminate\Support\ServiceProvider;
use EVT\API\Console\EvtApiGen;

class EvtApiServiceProvider extends ServiceProvider
{
    /**
     * @var
     */
    protected $configPath;
    /**
     * Bootstrap the application events.
     * @return void
     */
    public function boot()
    {
        $this->configPath = realpath( __DIR__ . '/../config/EVTAPI.php' );
        if ( function_exists( 'config_path' ) ) $publishPath = config_path( 'EVTAPI.php' ); else $publishPath = base_path( 'config/EVTAPI.php' );
        $this->publishes([ $this->configPath => $publishPath ], 'config' );

        if ($this->app->runningInConsole())
        {
            $this->commands([
                EvtApiGen::class,
            ]);
        }
    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        $this->configPath = realpath( __DIR__ . '/../config/EVTAPI.php' );
        $this->mergeConfigFrom( $this->configPath, 'EVTAPI' );
    }
}
